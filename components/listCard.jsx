/* eslint-disable react/prop-types */
import React from "react";
import Input from "./Input";

const ListCard = ({
  list,
  index,
  addItem,
  itemChecked,
  itemsTable,
  modalAction
}) => {
  const { title, items } = list;

  return (
    <article className="listCard">
      <h1>{title}</h1>
      <Input index={index} addItem={addItem} />
      <ul>
        {items.map(task => {
          return (
            <li key={task.id} index={task.id}>
              <span
                className={itemsTable[`${task.id}`].isCompleted ? "done" : ""}
              >
                {itemsTable[`${task.id}`].text}
              </span>
              <div>
                <button
                  className="button"
                  onClick={() => itemChecked(task.id)}
                  type="button"
                >
                  Done
                </button>
                <button
                  onClick={modalAction}
                  className="button is-danger"
                  type="button"
                >
                  {" "}
                  supprimer
                </button>
              </div>
            </li>
          );
        })}
      </ul>
      <style jsx>
        {`
          .listCard {
            display: inline-block;
            margin: 1rem;
          }
          h1 {
            text-align: center;
          }
          ul {
            list-style: none;
          }
          li {
            width: 100%;
            display flex;
            justify-content: space-between;
            margin: 1rem 0 1rem 0;
          }
          .done {
            text-decoration: line-through
          }
        `}
      </style>
    </article>
  );
};

export default ListCard;
