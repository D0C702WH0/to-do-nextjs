/* eslint-disable react/prop-types */
import React, { useState } from "react";

const Input = ({ addList, addItem, index }) => {
  /* Value of the input */
  const [title, setTitle] = useState("");

  /**
   * Allows to submit the input form
   * @param {*} e event of the input
   */
  const handleSubmit = e => {
    e.preventDefault();
    if (!title) return;
    if (addItem) {
      addItem(index, title);
    } else {
      addList({ title, items: [] });
    }
    setTitle("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="field has-addons">
        <div className="control">
          <input
            className={addItem ? "input is-warning" : "input is-primary"}
            type="search"
            name="title"
            required
            placeholder={addItem ? "Aouter une tâche" : "Ajouter une liste"}
            value={title}
            onChange={e => {
              e.preventDefault();
              setTitle(e.target.value);
            }}
          />
        </div>
        <div className="control">
          <button className="button is-info" type="submit">
            Ajouter
          </button>
        </div>
      </div>
      <style jsx>
        {`
          form: {
            display: flex;
          }
        `}
      </style>
    </form>
  );
};

export default Input;
