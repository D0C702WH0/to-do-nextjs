import React from "react";

const Modal = ({ isOpen, modalAction }) => {
  return (
    <div className={`modal ${isOpen}`}>
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Modal title</p>
          <button type="button" className="delete" aria-label="close" />
        </header>
        <section className="modal-card-body">
          Voulez vous supprimer cet élément?
        </section>
        <footer className="modal-card-foot">
          <button type="button" className="button is-success">
            confirmer
          </button>
          <button
            onClick={() => modalAction("close")}
            type="button"
            className="button"
          >
            annuler
          </button>
        </footer>
      </div>
    </div>
  );
};

export default Modal;
