import React, { useState } from "react";
import Head from "next/head";
import Input from "../components/Input";
import ListCard from "../components/listCard";
import Modal from "../components/modal";
import "../static/style.sass";

const Home = () => {
  /* Lists of todos */
  const [lists, setlists] = useState([]);
  const [itemsTable, setItemsTable] = useState({});
  const [toggleModal, useToggleModal] = useState("");

  /* Used to force re render */
  const [, setRerender] = useState();

  const modalAction = e => {
    if (e === "close") {
      return useToggleModal(" ");
    }
    return useToggleModal("is-active");
  };

  const createId = () => {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    return `${Math.random()
      .toString(36)
      .substr(2, 9)}`;
  };

  /**
   * Allows to create a new list of todos
   * @param {*} obj Object of the new list to add in the array
   */
  const addList = obj => {
    const newList = obj;
    newList.id = createId();
    setlists(() => [...lists, newList]);
    return setRerender({});
  };

  /**
   * Allows to add a new todo in a list
   * @param {*} listId Id of the list to update with new todo
   * @param {*} text Text of the new todo
   */
  const addItem = (listId, text) => {
    const targetList = lists.filter(e => e.id === listId);
    const id = createId();
    const newItem = {};
    newItem[id] = { text, isCompleted: false };
    setItemsTable({ ...itemsTable, ...newItem });
    return targetList[0].items.push({ id: `${id}` });
  };

  /**
   * Allows to change the isCompleted state of a todo item
   * @param {*} listId Id of the list that contains the todo item
   * @param {*} itemId Id of the item to update
   */
  // const itemChecked = (listId, itemId) => {
  //   lists.forEach(e => {
  //     if (updatedList.id === listId) {
  //       updatedList.items.forEach(f => {
  //         if (f.id === itemId) {
  //           f.isCompleted = !f.isCompleted;
  //         }
  //       });
  //     }
  //   });
  // };

  /**
   * Allows to change the isCompleted state of a todo item
   * @param {*} listId Id of the list that contains the todo item
   * @param {*} itemId Id of the item to update
   */
  // const itemChecked = (listId, itemId) => {
  //   lists.forEach(e => {
  //     if (updatedList.id === listId && updatedList.items.length) {
  //       const obj = updatedList.items.find(item => item.id === itemId);
  //       const objIndex = updatedList.items.indexOf(obj);
  //       const updatedObj = {
  //         ...updatedList.items[objIndex],
  //         isCompleted: !updatedList.items[objIndex].isCompleted
  //       };
  //       updatedList.items.splice(objIndex, 1);
  //       updatedList.items.push(updatedObj);
  //     }
  //   });
  // };

  // const listUpdater = (listId, itemId) => {
  //   const newList = lists.map((e) => {
  //     if (e.id === listId) {
  //       e.items.reduce((arr, v) => {
  //         if (v.id === itemId) {
  //           v.isCompleted = !v.isCompleted;
  //         }
  //         arr.push(v);
  //         return arr;
  //       }, []);
  //     }
  //     return e;
  //   });
  //   return newList;
  // };

  /**
   * Allows to change the isCompleted state of a todo item
   * @param {*} listId Id of the list that contains the todo item
   * @param {*} itemId Id of the item to update
   */
  // const itemChecked = (listId, itemId) => {
  //   const updatedLists = listUpdater(listId, itemId);
  //   lists.length = 0;
  //   return lists.push(...updatedLists);
  // };

  /**
   * Allows to change the isCompleted state of a todo item
   * @param {*} itemId Id of the item to update
   */
  const itemChecked = itemId => {
    itemsTable[itemId].isCompleted = !itemsTable[itemId].isCompleted;
    return setRerender({});
  };

  return (
    <div>
      <Head>
        <title>ToDo</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <link rel="stylesheet" href="../static/index.css" />
      </Head>

      <header>
        <Input addList={addList} />
      </header>
      <main>
        <Modal isOpen={toggleModal} modalAction={modalAction} />
        <section id="listContainer">
          {lists.map(v => {
            return (
              <ListCard
                itemsTable={itemsTable}
                addItem={addItem}
                key={v.id}
                index={v.id}
                list={v}
                itemChecked={itemChecked}
                modalAction={modalAction}
              />
            );
          })}
        </section>
      </main>
    </div>
  );
};

export default Home;
